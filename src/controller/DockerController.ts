import { DockerContainer, PrismaClient } from ".prisma/client";
import { randomUUID } from "crypto";

const Docker = require('dockerode');

export class DockerController {

    private _docker;
    private _prisma: PrismaClient;

    public get prisma(): PrismaClient {
        return this._prisma;
    }

    constructor(prisma: PrismaClient) {
        this._prisma = prisma;
        this._docker = new Docker({ socketPath: '/var/run/docker.sock' });
    }



    public async update() {
        updateDockerContainer(this._docker, this._prisma)();
    }

    /**
     * getAll
     */
    public async getAll() {
        return await this._prisma.dockerContainer.findMany();
    }

    /**
     * getOne
     */
    public async getOne(id: string) {
        return await this._prisma.dockerContainer.findUnique({
            where: {
                "id": id
            },
        })
    }

}

function updateDockerContainer(docker: any, prisma: PrismaClient) {

    return function () {
        let demo=process.env.DEMOMODE!=undefined
        let newContainer: DockerContainer[] = new Array<DockerContainer>();
        console.log('updateting container from Docker');
        docker.listContainers((err: any, containers: any[]) => {
            console.log("done");
            if (containers) {
                containers.forEach(async (containerInfo) => {
                    let dashEnabled = new String(containerInfo.Labels["dash.enabled"]).toLowerCase();
                    let composeService = containerInfo.Labels['com.docker.compose.service'];
                    console.log("Analysing: " + composeService + ': ' + dashEnabled);
                    if (dashEnabled !== 'false') {

                        let extractedURL = containerInfo.Labels['traefik.http.routers.' + composeService + '.rule'];
                        let dashUrl = containerInfo.Labels['dash.url'] ? containerInfo.Labels['dash.url'] : '';
                        let group = containerInfo.Labels['com.docker.compose.project'] ? containerInfo.Labels['com.docker.compose.project'] : undefined;

                        let name = containerInfo.Labels['dash.name'] ? containerInfo.Labels['dash.name'] : composeService;
                        let icon = containerInfo.Labels['dash.icon.mi'] ? containerInfo.Labels['dash.icon.mi'] : "public";
                        let color = containerInfo.Labels['dash.icon.color'] ? containerInfo.Labels['dash.icon.color'] : "#111111";

                        let url = '';
                        if (dashUrl != '') {
                            url = dashUrl;
                        } else {
                            const regex = /^Host\(\`(.*)\`\)/gm;
                            let match = regex.exec(extractedURL);
                            if (match) {
                                url = "http://" + match[1];
                            }
                        }

                        if (url != '') {
                            newContainer.push({
                                id: containerInfo.Id,
                                name: name,
                                exposedUrl: url,
                                icon: icon,
                                color: color,
                            })

                        }
                    }
                });

            }
            else if(demo){
                newContainer.push({
                    id: randomUUID(),
                    name: "demo1",
                    exposedUrl: "http://demo1.example.com",
                    icon: "mdiAccountCircle",
                    color: "#FFC107",
                })
                newContainer.push({
                    id: randomUUID(),
                    name: "demo2",
                    exposedUrl: "http://demo2.example.com",
                    icon: "mdiCloudTags",
                    color: "#2196F3",
                })
            }
            console.log("delete all containers");
            prisma.dockerContainer.deleteMany({})
                .then(() => {
                    console.log("done");
                    newContainer.forEach(async toCreate => {
                        let dbEntity = await prisma.dockerContainer.create({ data: toCreate });
                        console.log(dbEntity.name + " saved");
                    })
                })
        });

        let waitTime = 60;
        console.log(`running again in ${waitTime} seconds`);
        setTimeout(updateDockerContainer(docker, prisma), waitTime * 1000);
    }
}