# Dc Autodash

This is a dashboard for Docker. You create an entry by adding the following labels:

```yaml
  whoami:
    # A container that exposes an API to show its IP address
    image: traefik/whoami
    labels:
      - "dash.enabled=true"
      - "dash.icon.mi=mdiEarth"
      - "dash.icon.color=#FF0FF3"
      - "dash.url=http://whoami.docker.localhost"
```

Icons can be found <https://materialdesignicons.com/>. Please use the string from the `import` statement.

# Develop

start the backend and frontend dev-server by changing into project directory and typing:

```
npm start
```


Visit: <http://localhost:3000> and start testing

# Run prebuild docker image

docker run  -v /var/run/docker.sock:/var/run/docker.sock registry.gitlab.com/headbanger12/dc-autodash

docker-compose

```
  dash:
    build:
      context: .
      dockerfile: Dockerfile
    image: registry.gitlab.com/headbanger12/dc-autodash
    volumes:
      # - .:/usr/src/app/
      # So that Traefik can listen to the Docker events
      - /var/run/docker.sock:/var/run/docker.sock
    environment: 
      - "HOST=dash.docker.localhost"
    working_dir: /usr/src/app/
    labels:
      - "traefik.http.routers.dash.rule=Host(`dash.docker.localhost`)"
      - "dash.enabled=true"
      - "dash.icon.color=#AA0000"
      - "dash.icon.mi=mdiEarth"
```