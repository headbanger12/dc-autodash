import { IconButton,CssBaseline,AppBar,Toolbar,Typography } from '@mui/material';
import Icon from '@mdi/react';
import { mdiViewDashboard } from '@mdi/js'; 
import CircularProgress from '@mui/material/CircularProgress';
import React, { useEffect, useState } from 'react';
import ContainerList from './components/ContainerList';


function App() {

  const [containers, setContainers] = useState([]);
  const [fetching, setFetching] = useState(true);

  // Just on Initialisation
  useEffect(() => {
    getData();
  }, [])

  const getData = async () => {
    const response = await fetch('/api/v1/docker');
    const data = await response.json();
    console.log(data);
    setContainers(data);
    setFetching(false);
    setTimeout(getData,30*1000)
  }

  return (
    <React.Fragment>
      <CssBaseline />
      <AppBar position="static">
        <Toolbar>
          <IconButton edge="start" sx={{mr:2}} color="inherit" aria-label="menu">
             <Icon path={mdiViewDashboard} size={1}/>
          </IconButton>
          <Typography variant="h6" sx={{ flexgrow:1}}>
            Dashboard
          </Typography>
          {/* <Button color="inherit">Login</Button> */}
        </Toolbar>
      </AppBar>
      <Toolbar />
      <main>
        {fetching && <CircularProgress />}
        {!fetching && <ContainerList containers={containers} />}
      </main>

    </React.Fragment>

  );
}

export default App;
