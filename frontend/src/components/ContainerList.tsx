import { Container, Grid } from '@mui/material';
import * as React from 'react';
import DockerContainer, { IContainerProps } from './DockerContainer';

interface IContainerListProps {
    containers: any
}


const ContainerList: React.FunctionComponent<IContainerListProps> = (props) => {
    const containers = props.containers;

    return (
        <Container maxWidth="md">
            <Grid container spacing={4}>
                {containers.map((item: IContainerProps, index: any) => (
                    <Grid item key={index} xs={12} sm={6} md={4}>
                        <DockerContainer
                            id={item.id}
                            name={item.name}
                            exposedUrl={item.exposedUrl}
                            icon={item.icon}
                            color={item.color} />
                    </Grid>
                ))}
            </Grid>
        </Container>
    );
};

export default ContainerList;
