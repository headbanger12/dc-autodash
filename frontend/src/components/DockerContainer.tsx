import { Paper, Typography } from '@mui/material';
import Icon from '@mdi/react'
import * as mdiAllIcons from '@mdi/js';
import React from "react";
type MdiMapType = { [key: string]: string };


export interface IContainerProps {
    id: string;
    name: string;
    exposedUrl: string;
    icon: string;
    color: string;
}

export interface IContainerState {
}

export default class DockerContainer extends React.Component<IContainerProps, IContainerState> {


    private _mdi: MdiMapType;

    constructor(props: IContainerProps) {
        super(props);
        this._mdi = { ...mdiAllIcons };
    }

    public render() {
        return (
            <a href={this.props.exposedUrl}>
                <Paper sx={{ textAlign: 'center', padding:3.5 }} elevation={6} >
                    <Icon color={this.props.color} size={4} path={this._mdi[this.props.icon]} />
                    <Typography>
                        <strong>{this.props.name}</strong>
                    </Typography>
                </Paper>
            </a>
        );
    }
}
