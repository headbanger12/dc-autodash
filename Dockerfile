#######  Build Backend
 
FROM node:lts-alpine as node-modules

WORKDIR /usr/src/app/

RUN apk add --no-cache python3 make g++  sqlite sqlite-dev sqlite-libs 

COPY *.json ./
RUN npm install 
COPY . .
RUN npx prisma generate && npm run build

#######  Build Frontend

FROM node:lts-alpine as react

WORKDIR /usr/src/app/frontend/

COPY ./frontend/package*.json /usr/src/app/frontend/
RUN npm install

COPY ./frontend/ /usr/src/app/frontend/
RUN npm run build

#######  Build Container

FROM node:lts-alpine

WORKDIR /usr/src/app/

VOLUME [ "/data" ]

ENV DATABASE_URL="file:/data/database.db"

COPY --from=node-modules /usr/src/app/node_modules ./node_modules
COPY --from=node-modules /usr/src/app/dist ./dist
COPY --from=node-modules /usr/src/app/prisma ./prisma

COPY --from=react /usr/src/app/frontend/build ./public
COPY ./entrypoint.sh /usr/src/app/entrypoint.sh

EXPOSE 5000

ENTRYPOINT [ "/bin/sh","/usr/src/app/entrypoint.sh" ]